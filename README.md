horst makes it more easy to handle many depending SQL statements in a SQL script or SQL procedure.
The main support of horst is: Identical field lists in SELECT, INSERT, JOIN, WHERE, GROUP are managed
at ONE code line.

I created horst to manage the beast of data transformation with a chain of SQL statements in SQL procedures. 
In these procedures we use temporary tables to collect records, transform these records and write the results 
into the target table. Each additional new information aka field was a pain in the ass as we have to investigate
where to add these new field.
With horst it became a nice task.

# supported SQL languages
.Google BigQuery

# Extended BNF of horst

statement = table_declaration \
    | clone_table  \
    | drop tables  \
    | list_fields  \
    | join_fields  \
    | comment  

table_declaration = create table table_name field_list \
clone_table = clone table ID as ID {add field_list | without field_names}  \
list_fields = table_name {as table_alias} fields {with type} {without field_names}  \
join_fields = table_name {as table_alias} on table_name {as table_alias} {(with | without) field_names}  \
comment = -- * 

field_list = field_definition {, field_definition}  \
field_definition = field_name field_type  \
field_type = BOOL | INTEGER | STRING | TIMESTAMP  \
field_names = field_name {, field_name}  \
field_name = ID  \
table_name = ID  \
table_alias = ID  \
ID = RegEx([a-zA-Z_][a-zA-Z0-9_]*)  \

code example  \
$ python horst.py this_horst_script  \
file with horst code: horst.this_horst_script.sql  \
result sql file: this_horst_script.sql  
