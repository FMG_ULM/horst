-- That's the horst way.

-- create temp tables
{{ -- so kommentiert horst }}
{{ create table my_taby field_a integer, field_b integer, version_id string }}
-- clone table
{{ clone table my_taby as my_tab2 }}
{{ clone table my_taby as my_tab3 add field_add1 string }}

-- don't touch the others
SELECT Current_Date() Tag;

-- fields are now replaced
-- table_name                  fields
SELECT {{ my_taby fields }}
-- table_name                  fields             {without field_names}
SELECT {{ my_taby fields without field_a, field_b}}
-- table_name {as table_alias} fields             {without field_names}
SELECT {{ my_taby as dernun fields without field_b}}
-- table_name                  fields {with type} 
SELECT {{ my_taby fields with type}}
-- table_name                  fields {with type} {without field_names}
SELECT {{ my_taby fields with type without version_id}}
-- table_name {as table_alias} fields {with type} 
SELECT {{ my_taby as als fields with type}}
-- table_name {as table_alias} fields {with type} {without field_names}
SELECT {{ my_taby as als fields with type without field_b}}
FROM my_taby;

-- and a good join is also supported
-- join with
-- table_name {as table_alias} fields
SELECT  my_taby as soso fields 
FROM my_taby
JOIN my_tab2 as wto on {{ my_tab2 as wto on my_taby without field_b }};
change order 

-- drop horst tables
{{drop tables}}