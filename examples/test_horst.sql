-- That's the horst way.

-- create temp tables
CREATE TEMP TABLE my_taby (field_a INT64, field_b INT64, version_id string);
-- clone table
CREATE TEMP TABLE my_tab2 (field_a INT64, field_b INT64, version_id string);
CREATE TEMP TABLE my_tab3 (field_a INT64, field_b INT64, version_id string
, field_add1 string);

-- don't touch the others
SELECT Current_Date() Tag;

-- fields are now replaced
-- table_name                  fields
SELECT field_a, field_b, version_id
-- table_name                  fields             {without field_names}
SELECT version_id
-- table_name {as table_alias} fields             {without field_names}
SELECT dernun.field_a, dernun.version_id
-- table_name                  fields {with type} 
SELECT field_a INT64, field_b INT64, version_id string
-- table_name                  fields {with type} {without field_names}
SELECT field_a INT64, field_b INT64
-- table_name {as table_alias} fields {with type} 
SELECT als.field_a INT64, als.field_b INT64, als.version_id string
-- table_name {as table_alias} fields {with type} {without field_names}
SELECT als.field_a INT64, als.version_id string
FROM my_taby;

-- and a good join is also supported
-- join with
-- table_name {as table_alias} fields
SELECT  my_taby as soso fields 
FROM my_taby
JOIN my_tab2 as wto on my_taby.field_a = wto.field_a AND my_taby.version_id = wto.version_id;
change order 

-- drop horst tables
DROP TABLE my_taby;
DROP TABLE my_tab2;
DROP TABLE my_tab3;